<?php

namespace Drupal\content_moderation_revert;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\workflows\Entity\Workflow;

/**
 * Defines a class for dynamic permissions.
 *
 * @internal
 */
class Permissions {

  use StringTranslationTrait;

  /**
   * Returns an array of transition permissions.
   *
   * @return array
   *   The transition permissions.
   */
  public function revertPermissions() {
    $permissions = [];
    /** @var \Drupal\workflows\WorkflowInterface $workflow */
    foreach (Workflow::loadMultipleByType('content_moderation') as $id => $workflow) {
      foreach ($workflow->getTypePlugin()->getStates() as $state) {
        $args = [
          '%workflow' => $workflow->label(),
          '%state' => $state->label(),
        ];
        $permissions['revert ' . $workflow->id() . ' revisions to ' . $state->id()] = [
          'title' => $this->t('%workflow workflow: Revert revisions to %state state', $args),
          'description' => $this->t('For supported entity types assigned to the %workflow workflow, allows users to revert <em>any</em> content revision to the %state state. Reverting revisions will not consider any existing transition based permissions.', $args),
        ];
      }
    }
    return $permissions;
  }

}
